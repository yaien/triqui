package triqui

// Triqui struct for playing the game
type Triqui struct {
	Game   Game
	Turn   Player
	IsEnd  bool
	Result *Result
}

//Play make current turn move
func (triqui Triqui) Play(move int) *Result {
	if triqui.IsEnd {
		return triqui.Result
	}
	Play(triqui.Game, triqui.Turn, move)
	triqui.check()
	triqui.Turn = NextPlayerFor(triqui.Turn)
	return triqui.Result
}

func (triqui Triqui) check() {
	win := CheckWin(triqui.Game, triqui.Turn)
	if win != nil {
		triqui.Result = win
		triqui.IsEnd = true
	} else {
		draw := IsEnd(triqui.Game)
		if draw != nil {
			triqui.Result = draw
			triqui.IsEnd = true
		}
	}
}

//NewTriqui get a new triqui scenario
func NewTriqui() *Triqui {
	return &Triqui{
		Game: NewGame(),
		Turn: RandomPlayer(),
	}
}
