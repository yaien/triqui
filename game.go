package triqui

import (
	"errors"
)

// NewGame return new game values
func NewGame() Game {
	return make([]Player, 9)
}

// CheckWin check if the given player won
func CheckWin(game Game, player Player) *Result {
	for _, condition := range Conditions {
		win := true
		for _, box := range condition {
			if game[box] != player {
				win = false
				break
			}
		}
		if win == true {
			return WinnerResult(player)
		}
	}
	return nil
}

//IsEnd Check if the game is end
func IsEnd(game Game) *Result {
	for _, box := range game {
		if len(box) > 0 {
			return DrawResult()
		}
	}
	return nil
}

//Play Make a move
func Play(game Game, player Player, box int) {
	if box < 0 || box > len(game)-1 {
		panic(errors.New("Invalid move"))
	}
	game[box] = player
}

//WinnerResult win type result
func WinnerResult(player Player) *Result {
	return &Result{Type: "win", Winner: player}
}

//DrawResult draw type result
func DrawResult() *Result {
	return &Result{Type: "draw"}
}
