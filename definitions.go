package triqui

// Player Sign
type Player = string

// Game State
type Game = []Player

// Cross Player
const Cross = "Cross"

// Circle Player
const Circle = "Circle"

// Players array of players
var Players = []Player{Cross, Circle}

/*
Conditions to win given

0, 1, 2,

3, 4, 5,

6, 7, 8
*/
var Conditions = [][]int{
	{0, 1, 2},
	{3, 4, 5},
	{6, 7, 8},
	{0, 3, 6},
	{1, 4, 7},
	{2, 5, 8},
	{0, 4, 8},
	{2, 4, 6},
}

//Result final result of the game
type Result struct {
	Type   string
	Winner Player
}
