package triqui

import (
	"errors"
	"math/rand"
)

// RandomPlayer get a random player
func RandomPlayer() Player {
	index := rand.Intn(len(Players))
	return Players[index]
}

// NextPlayerFor return next player of the current player
func NextPlayerFor(current Player) Player {
	pos := -1

	for index, player := range Players {
		if player == current {
			pos = index
			break
		}
	}

	if pos == -1 {
		panic(errors.New("Invalid Current Player"))
	}

	if pos == len(Players)-1 {
		return Players[0]
	}

	return Players[pos+1]
}
